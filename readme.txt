2D pixel-art game assets

More than 150 game objects in PSD and PNG
Tile size: 32x32
Background: Transparent

For TMX ( tileset.tmx )
tiled sprite sheet
Best for Tiled Map Editor
Download free at: http://www.mapeditor.org/

For question contact me at:
games.redgear@gmail.com